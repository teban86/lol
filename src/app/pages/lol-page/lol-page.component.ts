import { Component, OnInit } from '@angular/core';
import { Perso } from 'src/app/models/perso';
import { LolService } from 'src/app/services/lol.service';

@Component({
  selector: 'app-lol-page',
  templateUrl: './lol-page.component.html',
  styleUrls: ['./lol-page.component.css']
})
export class LolPageComponent implements OnInit {

  persos: Perso[]= [];

  constructor(private servicelol: LolService) { }

  ngOnInit(): void {
    this.servicelol.getPerso();
    this.getPersos();
  }

  getPersos = (): void => {
    this.servicelol.persoStream.subscribe(
      data => {
        this.persos = data
      },
      err => console.log(err)
    )
  }

  deletePerso = (data: number):void => {
    this.servicelol.deletePerso(data).subscribe(
      data => {
        this.servicelol.getPerso();
      },
      err => console.log(err)
    )
  }

  addPerso = (data: Perso): void => {
    this.servicelol.addPerso(data).subscribe(
      data => {
        this.servicelol.getPerso();
      },
      err => console.log(err)
    )
  } 

  changePerso = (data: Perso): void => {
    this.servicelol.changeStateActif(data).subscribe(
      data => {
        this.servicelol.getPerso();
      },
      err => console.log(err)
    )
  }

}
