import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolPageComponent } from './lol-page.component';

describe('LolPageComponent', () => {
  let component: LolPageComponent;
  let fixture: ComponentFixture<LolPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
