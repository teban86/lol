import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Perso } from 'src/app/models/perso';

@Component({
  selector: 'app-add-perso',
  templateUrl: './add-perso.component.html',
  styleUrls: ['./add-perso.component.css']
})
export class AddPersoComponent implements OnInit {

  @Output() newPerso: EventEmitter<Perso> = new EventEmitter<Perso>();

  tittle: string = "";
  key: string = "";
  name: string = "";
  id: number = 0;
  active:boolean = false;

  perso : Perso = {
    title: "",
    id:0,
    key:"",
    name: "",
    active: false
  }

  constructor() { }

  ngOnInit(): void {
  }

  addPerso = (): void => {
    this.perso = {
      title: this.tittle,
      id: this.id,
      key: this.key,
      name: this.name,
      active:this.active
    }
    
    this.newPerso.emit(this.perso);
    this.name = "";
    this.active = false;
  }

}
