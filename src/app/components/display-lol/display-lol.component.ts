import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Perso } from 'src/app/models/perso';

@Component({
  selector: 'app-display-lol',
  templateUrl: './display-lol.component.html',
  styleUrls: ['./display-lol.component.css']
})
export class DisplayLolComponent implements OnInit {

  @Input() persos: Perso[]=[];
  @Output() deletePerso:EventEmitter<number> = new EventEmitter<number>()
  @Output() changePerso:EventEmitter<Perso> = new EventEmitter<Perso>()

  constructor() { }

  ngOnInit(): void {
  }

  deletePersoFromChild = (data: number) => {
    this.deletePerso.emit(data);
  }

  changeState = (data: Perso):void => {
    data.active == false ? data.active = true : data.active = false
    this.changePerso.emit(data)
}

}
