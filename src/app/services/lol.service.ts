import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { Perso } from '../models/perso';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LolService {

  persoStream = new BehaviorSubject<Perso[]>([]);


  constructor(private http: HttpClient) { }

  getPerso = () :void => {
    this.http.get<Perso[]>(environment.URL).subscribe(
      data => {
        this.persoStream.next(data);
      }
    )
  }

  deletePerso = (id: number): Observable<Perso[]> => {
    return this.http.delete<Perso[]>(`${environment.URL}/${id}`);
  }

  addPerso = (data: Perso): Observable<Perso> => {
    return this.http.post<Perso>(environment.URL, data)
  }

  changeStateActif = (data: Perso): Observable<Perso> => {
    return this.http.patch<Perso>(`${environment.URL}/${data.id}`, data)
  }

}
