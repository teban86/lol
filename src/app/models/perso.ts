export interface Perso {
    title: string,
    id: number,
    key: string,
    name: string,
    active: boolean
}